const path = require('path');
const fs = require('fs');
const {
    duplicateObjectDTO,
} = require('./dto');

function isValidFile(fullPath) {
    let isFile = false;
    try {
        const statResult = fs.statSync(fullPath);
        isFile = statResult.isFile();
    } catch (err) {
        const expectedErrors = ['ENOENT', 'ENOTDIR'];
        if (!expectedErrors.includes(err.code)) {
            console.log(err);
        }
    }
    return isFile;
}

function getFilesForDeletion(duplicateObj) {
    const invalidFiles = []
    const filesToDelete = [];
    for (const filename of Object.keys(duplicateObj)) {
        let invalidPaths = false;
        const searchResult = duplicateObjectDTO({
            startFolder: duplicateObj[filename].startFolder,
            startFullPath: duplicateObj[filename].startFullPath,
            duplicatePaths: duplicateObj[filename].duplicatePaths,
        });

        invalidPaths = isValidFile(searchResult.startFolder);
        invalidFiles.push(filename);

        // If the source file is valid, check validity of duplicate paths
        if (!invalidPaths) {
            for (const duplicatePath of searchResult.duplicatePaths) {
                if (isValidFile(duplicatePath)) {
                    filesToDelete.push(duplicatePath);
                } else {
                    invalidFiles.push(duplicatePath);
                }
            }
        }
    }
    return {
        filesToDelete,
        invalidFiles,
    };
}

function deleteDuplicateFiles(duplicateObj) {
    const { filesToDelete } = getFilesForDeletion(duplicateObj);
    for (const duplicate of filesToDelete) {
        console.log('Removing: ', duplicate);
        try {
            fs.unlinkSync(duplicate);
        } catch (err) {
            if (!err.code === 'ENOENT') {
                console.error(err);
            } else {
                console.log('File already removed');
            }
        }
        console.log('');
    }
}

module.exports = {
    getFilesForDeletion,
    deleteDuplicateFiles,
}

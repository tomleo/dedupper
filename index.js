#!/usr/bin/env node

const fs = require('fs');
const path = require('path');

const { writeDuplicates } = require('./find-duplicates');
const { deleteDuplicateFiles } = require('./remove-duplicates');

const helpMain = () => process.stdout.write(`deduper

USAGE:
    dedupper <command> [<args>]

`);

const helpBuild = () => process.stdout.write(`deduper

USAGE:
    dedupper build [<options>]

OPTIONS:
    -f, --file           output JSON file
    -s, --start          folder to use as source of truth
    -t, --target         folder to look for duplicates

`);

const helpDelete = () => process.stdout.write(`deduper

USAGE:
    deduper delete [<options>]

OPTIONS:
    -f, --file           JSON file to process

`);

// -----------------------------------------------------

const dir = path.join(process.cwd());
const argv = require('minimist')(process.argv.slice(2));

if (!argv._ || !argv._[0]) {
    helpMain();
    return;
}

const subApp = argv._[0];
if (!['build', 'delete'].includes(subApp)) {
    helpMain();
    return;
}

if (subApp === 'build') {
    let file = argv.f || argv.file;
    let start = argv.s || argv.start;
    let target = argv.t || argv.target;
    if (!file && !start && !target) {
        helpBuild();
        return;
    }

    if (!path.isAbsolute(file)) {
        file = path.resolve(path.join(dir, file));
    }
    if (!path.isAbsolute(start)) {
        start = path.resolve(path.join(dir, start));
    }
    if (!path.isAbsolute(target)) {
        target = path.resolve(path.join(dir, target));
    }

    console.log(`finding duplicates in (target) ${target}`);
    console.log(`that are already in (start) ${start}`);
    console.log(`and writing the results to ${file}`);
    writeDuplicates(start, target, file);
} else {
    const file  = argv.f || argv.file;
    if (!file) {
        helpDelete();
        return;
    }
    const rawData = fs.readFileSync(path.resolve(file));
    const dupObject = JSON.parse(rawData);

    console.log(`deleteDuplicateFiles(file: ${file})`);
    deleteDuplicateFiles(dupObject);
}

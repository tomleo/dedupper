#!/usr/bin/env node

const path = require('path')
const fs = require('fs');
const { promisify } = require('util');
const exec = promisify(require('child_process').exec)
const {
        searchResultDTO,
        duplicateObjectDTO,
} = require('./dto');

async function findFile(filename, targetDir) {
    return await exec(`fdfind "${filename}" "${targetDir}"`);
}

async function* walk(dir) {
    for await (const d of await fs.promises.opendir(dir)) {
        const entry = path.join(dir, d.name);
        if (d.isDirectory()) yield* walk(entry);
        else if (d.isFile()) yield entry;
    }
}

async function* matches(startPath, targetDir) {
    for await(const p of walk(startPath)) {
        const filename = path.basename(p);
        const folder = path.dirname(p);
        try {
            const result = await findFile(filename, targetDir);
            stdout = result.stdout;
            stderr = result.stderr;
        } catch (err) {
            stderr = err;
        }
        if (stdout) {
            const matchFullPaths = `${stdout}`.split('\n');
            yield searchResultDTO({
                startFolder: folder,
                startFilename: filename,
                startFullPath: p,
                matchFullPaths,
            })
        }
    }
}

async function buildDuplicateObject(startPath, targetDir) {
    const duplicateObject = {};
    for await(const result of matches(startPath, targetDir)) {
        if (!duplicateObject[result.startFilename]) {
            duplicateObject[result.startFilename] = duplicateObjectDTO({
                startFolder: result.startFolder,
                startFullPath: result.startFullPath,
                duplicatePaths: new Set(),
            });
        }
        result.matchFullPaths.filter((i) => i).forEach((m) => {
            duplicateObject[result.startFilename].duplicatePaths.add(m);
        });
        // duplicateObject[result.startFilename].duplicatePaths = duplicateObject[result.startFilename].duplicatePaths.concat(result.matchFullPaths.filter((i) => i));
    }
    return Promise.resolve(duplicateObject);
}

async function writeDuplicates(startPath, targetDir, outputFileName) {
    const duplicateObj = await buildDuplicateObject(startPath, targetDir);
    fs.writeFile(outputFileName, JSON.stringify(
        duplicateObj,
        (_key, value) => (value instanceof Set ? [...value] : value), // replacer to handle sets
        4,
    ), function(err) {
        if (err) {
            return console.log(err);
        }
        console.log(`file ${outputFileName} written!`);
    });
}

module.exports = {
    walk,
    matches,
    buildDuplicateObject,
    writeDuplicates,
}

function searchResultDTO({
    startFolder,
    startFilename,
    startFullPath,
    matchFullPaths,
}) {
    return {
        startFolder,
        startFilename,
        startFullPath,
        matchFullPaths,
    }
}

function duplicateObjectDTO({
    startFolder,
    startFullPath,
    duplicatePaths,
}) {
    return {
        startFolder,
        startFullPath,
        duplicatePaths,
    }
}

module.exports = {
    searchResultDTO,
    duplicateObjectDTO,
}